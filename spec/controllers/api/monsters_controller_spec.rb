# RSpec.describe Api::V1::MonstersController do
#   describe "GET index" do

#     context 'with login' do
#       before do
#         @user = create(:user)
#         @monster = create(:monster, user: @user, monster_type: 'fire')
#         sign_in @user
#       end

#       it "should return monsters json" do
#         get :index, format: :json
#         expected_response = {success: true, monsters: [@monster].as_json}
#         expect(response.body).to eq(expected_response.to_json)
#       end
#     end
#   end

#   describe "#create" do

#     context 'with login' do
#       before do
#         @user = create(:user)
#         sign_in @user
#       end

#       it "creates a monster" do
#         post :create, monster: attributes_for(:monster), format: :json
#         expected_response = {success: true, monster: Monster.last.as_json}
#         expect(response.body).to eq(expected_response.to_json)
#       end
#     end
#   end

#   describe "#update" do

#     context 'with login' do
#       before do
#         @user = create(:user)
#         sign_in @user
#         @monster = create(:monster, user: @user)
#       end

#       it "creates a monster" do
#         post :update, id: @monster.id, monster: {name: 'Monster1o1'}, format: :json
#         expected_response = {success: true, monster: Monster.last.as_json}
#         expect(response.body).to eq(expected_response.to_json)
#       end
#     end
#   end

#   describe "#destroy" do

#     context 'with login' do
#       before do
#         @user = create(:user)
#         sign_in @user
#         @monster = create(:monster, user: @user)
#       end

#       it "destroy the monster" do
#         delete :destroy, id: @monster.id, format: :json
#         expected_response = {success: true}
#         expect(response.body).to eq(expected_response.to_json)
#       end
#     end
#   end

# end
