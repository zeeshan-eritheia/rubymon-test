require 'rails_helper'

RSpec.describe User, type: :model do
  context 'association' do
    it { should have_many(:teams) }
    it { should have_many(:monsters) }
  end
end