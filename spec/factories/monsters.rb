FactoryGirl.define do
  factory :monster do
    sequence :name do |n|
      "Monster #{n}"
    end
    power "MyString"
    monster_type "wind"
    user_id 1
  end

end
