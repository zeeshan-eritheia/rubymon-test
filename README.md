# Rubymon Test

# Quick install guide

1. Install dependencies (see below)

2. Clone git repository

   git clone git@bitbucket.org:zeeshan-eritheia/rubymon-test.git

3. Create database and user, as specified by Postgres settings in GAC-Rails/config/databse.yml.


4. Install gems

        cd rubymon-test
        bundle install
        git checkout bin/

5. Apply schema on test database (Note: for development db, it is best to use staging copy.)

        bundle exec rake db:schema:load


6. To visit site on http://localhost:3000

        bundle exec rails s

7. Used rails best practices
     
       cd rubymon-test
       rails_best_practices .

8. To run specs
     
       cd rubymon-test
       bundle exec rake spec

# API framework(Have not used any framework like grape for this test assignment)
    
    Added simple CRUD operations for teams and monsters
    
    1. Response from web app controllers: 
       http://localhost:3000/monsters.json

    2. From API controllers  
       http://localhost:3000/api/v1/monsters.json


# Writing code

## Text editor settings:

1. Used space as delimiter and 2 spaces per indent.
2. Used LF EOL marker "\n"
3. Clean trailing whitespace and EOL markers before file save.
4. Ensured file ends with EOL marker before file save.


## Coding practices

1. Refactored the code, as needed
2. Followed KISS principle.
3. Less code == Less errors.