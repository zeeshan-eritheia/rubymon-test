class TeamsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_team, only: [:show, :edit, :update, :destroy]

  def index
    @teams = current_user.teams
    respond_to do |format|
      format.html {}
      format.json { render json: {success: true, teams: @teams.as_json }}
    end
  end

  def show
  end

  def new
    @team = Team.new
    @monsters = current_user.monsters.where('team_id IS NULL')
  end

  def create
    @team = current_user.teams.create(team_params)
    unless params[:monsters].blank?
      params[:monsters].each do |monster_id|
        @monster = Monster.find(monster_id)
        @monster.update(team_id: @team.id)
      end
    end
    redirect_to monsters_path
  end

  def edit
  end

  def update
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to @team, notice: 'Team was successfully updated.' }
        format.json { render json: {success: true, team: @team.as_json } }
      else
        format.html { render :edit }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url, notice: 'Team was successfully destroyed.' }
      format.json { render json: {success: true} }
    end
  end

  private
   def set_team
     @team = current_user.teams.find(params[:id])
   end

   def team_params
     params.require(:team).permit(:name, :user_id)
   end
end