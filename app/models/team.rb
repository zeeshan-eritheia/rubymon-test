class Team < ActiveRecord::Base
  has_many :monsters, dependent: :nullify
  belongs_to :user

  validates :name, presence: true, uniqueness: {scope: :user_id}
  validate :available_teams, on: :create

  def allows_new_monster?
    self.monsters.count < 3
  end

  private
  def available_teams
    unless user.allows_new_team?
      errors.add(:base, 'Reached the number of teams')
    end
  end

end